#!/bin/bash

# Set Local Variables for Frontend Server
PUBIP=$(curl --silent http://169.254.169.254/latest/meta-data/local-ipv4)
PUBNET=$(netstat -rn | grep 172.31 | grep 240.0 | grep eth0 | awk '{print $1}')
PUBMASK=$(/sbin/ifconfig eth0 | grep Mask | awk -F\: '{print $4}')
PUBCIDR=$(ip addr show dev eth0 | grep "global eth0" | awk -F\. '{print $4}' | cut -d ' ' -f 1 | awk -F\/ '{print $2}')
PUBBC=$(ifconfig eth0 | grep Bcast | cut -d : -f 3 | awk '{ print $1 }')
PRIVIP=$(/sbin/ifconfig eth1 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
PRIVNET=$(netstat -rn | grep 172.31 | grep 240.0 | grep eth1 | awk '{print $1}')
PRIVBC=$(ifconfig eth1 | grep Bcast | cut -d : -f 3 | awk '{ print $1 }')
PRIVMASK=$(/sbin/ifconfig eth1 | grep Mask | awk -F\: '{print $4}')
PRIVCIDR=$(ip addr show dev eth1 | grep "global eth1" | awk -F\. '{print $4}' | cut -d ' ' -f 1 | awk -F\/ '{print $2}')
DEFGW=$(netstat -rn | grep UG | awk '{print $2}')
MYHOST=$(hostname)

# Set rocks global values
rocks set attr attr=Kickstart_PrivateAddress value=$MYHOST
rocks set attr attr=Kickstart_PrivateBroadcast value=172.31.255.255
rocks set attr attr=Kickstart_PrivateDNSDomain value=$(grep search /etc/resolv.conf | awk '{ print $2 }')
rocks set attr attr=Kickstart_PrivateDNSServers value=$(grep nameserver /etc/resolv.conf | awk '{ print $2 }')
rocks set attr attr=Kickstart_PrivateGateway value=$PRIVIP
rocks set attr attr=Kickstart_PrivateKickstartHost value=$PRIVIP
rocks set attr attr=Kickstart_PrivateNTPHost value=$PRIVIP
rocks set attr attr=Kickstart_PrivateNetmask value=$PRIVMASK
rocks set attr attr=Kickstart_PrivateNetmaskCIDR value=$PRIVCIDR
rocks set attr attr=Kickstart_PrivateNetwork value=$PRIVNET
rocks set attr attr=Kickstart_PrivateSyslogHost value=$PRIVIP

rocks set attr attr=Kickstart_PublicAddress value=$PUBIP
rocks set attr attr=Kickstart_PublicBroadcast value=$PUBBC
rocks set attr attr=Kickstart_PublicDNSServers value=$(grep nameserver /etc/resolv.conf | awk '{ print $2 }')
rocks set attr attr=Kickstart_PublicGateway value=$DEFGW
rocks set attr attr=Kickstart_PublicNetmask value=$PUBMASK
rocks set attr attr=Kickstart_PublicNetmaskCIDR value=$PUBCIDR
rocks set attr attr=Kickstart_PublicNetwork value=$PRIVNET
rocks set attr attr=dhcp_nextserver value=$PRIVIP

# Find out Remote Values for Hosts
COMPUTENODES=$(aws ec2 describe-instances --filters Name=tag:NodeType,Values=compute --region eu-west-1 | grep PrivateDnsName | awk -F\" '{print $4}' | uniq | awk -F\. '{print $1}')

# Configure Networks and Routes
rocks add network public netmask=$PUBMASK subnet=$PUBNET
rocks add network private netmask=$PRIVMASK subnet=$PRIVNET
rocks add route 0.0.0.0 $DEFGW netmaks=0.0.0.0

# Setup Frontend Node
rocks add host $MYHOST cpus=1 membership=Frontend rack=0 rank=0
rocks add host interface $MYHOST iface=eth0 ip=$PUBIP module=Frontend name=$MYHOST subnet=public
rocks add host interface $MYHOST iface=eth1 ip=$PRIVIP module=Frontend name=$MYHOST subnet=private
rocks sync host network
rocks sync config

# Add the cluster nodes
for NODE in ${COMPUTENODES}; do
	set NODEIP='';
	NODEIP=$(host $NODE | awk '{print $4}');
	rocks add host $NODE cpus=1 membership=Compute rack=0 rank=0;
	rocks add host interface $NODE iface=eth0 ip=$NODEIP module=Compute name=$NODE subnet=private;
	rocks set host attr $NODE attr=HttpConf value=/etc/httpd/conf
	rocks set host attr $NODE attr=Kickstart_PrivateAddress value=$PRIVIP
	rocks set host attr $NODE attr=Kickstart_PrivateBroadcast value=172.31.255.255
	rocks set host attr $NODE attr=Kickstart_PrivateDNSDomain value=$(grep search /etc/resolv.conf | awk '{ print $2 }')
	rocks set host attr $NODE attr=Kickstart_PrivateDNSServers value=$(grep nameserver /etc/resolv.conf | awk '{ print $2 }')
	rocks set host attr $NODE attr=Kickstart_PrivateGateway value=$PRIVIP
	rocks set host attr $NODE attr=Kickstart_PrivateKickstartHost value=$PRIVIP
	rocks set host attr $NODE attr=Kickstart_PrivateNTPHost value=$PRIVIP
	rocks set host attr $NODE attr=Kickstart_PrivateNetmask value=$PRIVMASK
	rocks set host attr $NODE attr=Kickstart_PrivateNetmaskCIDR value=$PRIVCIDR
	rocks set host attr $NODE attr=Kickstart_PrivateNetwork value=$PRIVNET
	rocks set host attr $NODE attr=Kickstart_PrivateSyslogHost value=$PRIVIP
	# Set Public Details
	rocks set host attr $NODE attr=Kickstart_PublicAddress value=$PUBIP
	rocks set host attr $NODE attr=Kickstart_PublicBroadcast value=$PUBBC
	rocks set host attr $NODE attr=Kickstart_PublicDNSServers value=$(grep nameserver /etc/resolv.conf | awk '{ print $2 }')
	rocks set host attr $NODE attr=Kickstart_PublicGateway value=$DEFGW
	rocks set host attr $NODE attr=Kickstart_PublicNetmask value=$PUBMASK
	rocks set host attr $NODE attr=Kickstart_PublicNetmaskCIDR value=$PUBCIDR
	rocks set host attr $NODE attr=Kickstart_PublicNetwork value=$PRIVNET
	rocks set host attr $NODE attr=dhcp_nextserver value=$PRIVIP
	# Set other options
	rocks set host boot $NODE action=os
	set NODEIP=''; 
done
