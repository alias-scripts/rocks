#!/bin/bash

# Set Local Variables for Frontend Server
PUBIP=$(curl --silent http://169.254.169.254/latest/meta-data/local-ipv4)
PUBNET=$(netstat -rn | grep 172.31 | grep 240.0 | grep eth0 | awk '{print $1}')
PUBMASK=$(/sbin/ifconfig eth0 | grep Mask | awk -F\: '{print $4}')
PRIVIP=$(/sbin/ifconfig eth1 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
PRIVNET=$(netstat -rn | grep 172.31 | grep 240.0 | grep eth1 | awk '{print $1}')
PRIVMASK=$(/sbin/ifconfig eth1 | grep Mask | awk -F\: '{print $4}')
DEFGW=$(netstat -rn | grep UG | awk '{print $2}')
MYHOST=$(hostname)

# Remove Cluster Nodes
HOSTS=$(rocks list host | grep 'Compute' | awk -F\: '{print $1}')
for NODE in ${HOSTS}; do
	set NODE='';
	rocks remove host $NODE
	set NODE=''; 
done

# Remove Frontend Node
rocks remove host interface $MYHOST iface=eth0
rocks remove host interface $MYHOST iface=eth1
rocks remove host $MYHOST

# Remove Networks and Routes
rocks remove route 0.0.0.0
rocks remove network public
rocks remove network private

# Perform a config sync
rocks sync config
